from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import authenticate, login
from rest_framework.response import Response
from rest_framework.views import APIView
from aps_api.serializers.loginSerializers import LoginSerializers
from rest_framework import status
from aps_api.properties.request import mss


class LoginView(APIView):
    def post(self, request):
        serializer = LoginSerializers(data=request.data)
        if serializer.is_valid():
            username = serializer.validated_data['username']
            password = serializer.validated_data['password']
            user = authenticate(request=request._request, username=username, password=password)

            # Verifica si el usuario no es None y si la propiedad 'true' existe y es verdadera
            if user is not None:
                login(request, user)
                pollster = user.account
                groups = user.groups.all()
                pollster_data = {'id': pollster.id, 'name': pollster.name_person.name}
                groups_data = [{'name': group.name, 'id': group.id} for group in groups]

                return Response({"pollster": {"pollster": pollster_data, "grupo": groups_data}},
                                status=status.HTTP_200_OK)
            else:
                return Response({"error": "Usuario o contraseña incorrectos"}, status=status.HTTP_401_UNAUTHORIZED)
        return Response(serializer.errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
