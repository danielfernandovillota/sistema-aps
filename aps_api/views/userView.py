from django.contrib.auth import logout
from django.contrib.auth.models import User
from rest_framework.decorators import api_view
from rest_framework.response import Response
from aps_api.serializers.userRegisterSerializers import UserRegisterSerializers
from rest_framework import status
from aps_api.properties.request import mss
from django.middleware.csrf import get_token
from aps_api.utils.querys import post_request


@api_view(['POST'])
def register(request):
    try:
        return post_request(request, User, UserRegisterSerializers)
    except Exception as e:
        return Response({mss[1]: str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
def out(request):
    try:
        logout(request)
        return Response({mss[7]}, status=status.HTTP_200_OK)
    except Exception as e:
        return Response({mss[1]: str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
def token_generate(request):
    token = get_token(request)
    return Response({'csrf_token': token}, status=status.HTTP_200_OK)



