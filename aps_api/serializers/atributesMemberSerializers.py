from rest_framework import serializers
from aps_api.managers.atributesMember import AtributesMember


class AtributesMemberSerializers(serializers.ModelSerializer):
    class Meta:
        model = AtributesMember
        fields = '__all__'


class CustomSerializers(serializers.ModelSerializer):
    class Meta:
        model = AtributesMember
        fields = '__all__'


class CustomUpdateSerializers(serializers.ModelSerializer):
    class Meta:
        model = AtributesMember
        read_only_fields = ['serial_id', 'member']
