from rest_framework import serializers
from django.contrib.auth.models import User, Group


class UserRegisterSerializers(serializers.ModelSerializer):
    groups = serializers.PrimaryKeyRelatedField(many=True, queryset=Group.objects.all())

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'groups']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        usuario = User.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password']
        )
        usuario.set_password(validated_data['password'])
        usuario.save()

        # Asignar el usuario a los grupos seleccionados
        grupos = validated_data.pop('groups')
        usuario.groups.set(grupos)

        return usuario
