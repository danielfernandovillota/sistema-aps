from aps_api.managers.pollster import Pollster
from aps_api.serializers.contactSerializers import ContactMemberSerializers
from aps_api.serializers.nameSerializers import NameSerializers, NameMemberSerializers
from aps_api.utils.serializersGeneric import SerializerEnlace


class PollsterSerializers(SerializerEnlace):
    class Meta:
        model = Pollster
        fields = '__all__'


class CustomSerializers(SerializerEnlace):
    class Meta:
        model = Pollster
        fields = ['name_person', 'serial_id', 'contact']


class CustomUpdateSerializers(SerializerEnlace):
    name_person = NameMemberSerializers()
    contact = ContactMemberSerializers()
    class Meta:
        model = Pollster
        fields = ['name_person','contact']


class NombreSerializer(SerializerEnlace):
    name_person = NameSerializers()
    class Meta:
        model = Pollster
        fields = ['name_person']
